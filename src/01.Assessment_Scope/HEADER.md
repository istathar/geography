# Assessment Scope

This assessment is an examination that lasts 40 minutes.  It consists of the following topics

* Water in the World
* Place and Liveability
* Interconnections
* Geography Skills
  * Topographic Maps (contour lines, area and grid referencing, direction, distance)
  * Synoptic Charts
  * Population Pyramids
  * Climate Graphs

The examination will consist of two sections

1. Multiple choice questions
1. Short answer questions
