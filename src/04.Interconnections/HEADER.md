# Interconnections

## Tourism

### Future Tourism

* Future tourists are more mature and experience
* They want value for money
* They adopt new technologies
* They have concern for safety and security
* They benefit from increased competition
* They look for more experiences than services
* They look for sustainable tourism and development

### Medical Tourism

Medical tourism is when people travel overseas for surgical procedures.  Asia is the leader in this area,
particularly Thailand and India.  People do this to avoid lengthy waiting lists and for other reasons

* cosmetic surgery
* knee replacements
* hip replacements
* fertility treatments
* surrogacy services
* complex heart surgery
* high standards of care or outstanding reputation for particular procedure
* cost savings
* reduced or no waiting time
* opportunity to include a holiday as part of the package

### Sport, Recreation and Leisure

Major sporting events like the Olympics and high profile regular sporting events attract tourists, cause
infrastructure to be built and can have big financial benefits for the hosts

### Cultural Tourism

Cultural tourism is concerned with the way of life in a particular region and the effects it has on
tourism levels.  Examples of cultural tourism are

* People travelling home for Thanksgiving in America
* Chinese New Year festivals

### Negative Effects of Tourism

* Tourism damages the environment and depletes resources
* Revenue generated does not stay in the local economy but goes to overseas companies like hotels
* Tourism damages culture and community and exploits locals
* In some countries, local people employed in tourism earh about 1% of the income of their guests

### Positive Effects of Tourism

* Provides employment and reduces poverty
* Increases interest in the natural environment
* Improves healthcare in poorer countries
* Add lots of money to the global enconomy
* To cope with the demands, additional investments in infrastructure need to be made
* Encourages restoration of historical sites
* Preserves indigenous cultures and heritage because tourist dollars encourage conservation of sites
* Increases awareness of impact actions on the environment

### Future Trends

* Space tourism
* Tackling the dilemma of sustainability, as air travel currently produces 2% of all global carbon emissions
* Ecotourism
  * recognises that many people want to learn about the natural environment
  * Aims to limit the impact of tourist facilities

## Technology Connecting People

### Transport Changes

* Planes and air travel has become more accessible
  * greater number of passengers able to travel at once
  * online access to discount information
  * reduced travel times
  * increased comfort
  * increase reliability and safety
  * better services (like inflight entertainment)
* There is a shift towards more efficient, hybrid and electric automobiles
* Shipping has changed with larger and larger container ships
* Public transport is a key component and is attracting large infrastructure spending

## Communication

* Internet
* Web page standards have improved to adapt to more modern devices
* Mobile phones have become more powerful
* Digital economy and online services continue to grow
  * Online shopping
  * Business
  * Education
  * Environment (monitoring resource usage, etc)

## Trade

Trade is another awy of connecting us

* Trade in food
* Fair trade is important for social justice because of potential exploitation that can arise
