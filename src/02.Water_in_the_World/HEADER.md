# Water in the World

The amount of water in the world is fixed and has not changed since the beginning of time.  However, water is
still viewed as **renewable** because it is constantly changing state from gas, to liquid, to solid, and back.  If it is properly managed, it can be used and recycled continuously.

The water cycle consists of

* infiltration (rain soaking into the ground)
* groundwater flow
* run-off (rain landing on the ground and flowing over it)
* transpiration (taken up by plants and released back as water vapour)
* evaporation (from water surfaces and the soil)
* condensation (water vapour forming clouds as it rises and cools in the air)
* precipitation (condensation drops increase in size and fall back to earth mainly rain, but also snow and hail)
* freezing (eg. on mountains)

## Distribution of Water Resources

### Groundwater

* 2% of the Earth's water is groundwater (94% is the ocean, 0.1% as rivers and lakes, the rest is vapour and
  other forms)
* Groundwater is between water-bearing rocks or **aquifers** in the ground, which act like sponges
* An **artesian aquifer** occurs between impermeable (not spongy) rocks and causes great pressure.  If a
  hole is bored to them from the surface, the water may gush to the surface from the pressure
* Groundwater is replenished when water seeps from the surface into the aquifers.  This is called
  **groundwater recharge**

### Natural Factors that Affect Water Availability

* Latitude (it is hotter and more humid near the equator, so more precipitation, so more rain)
* Altitube (as altitude increases, so does rainfall and snow because it is cooler at high altitudes)
* Ocean currents (warm currents evaporate easily and result in higher rainfall than colder currents)
* Distance from the sea (areas close to the sea get more rain)
* Geology (rocks that are permeable hold water)
* Topography (on the windward side of mountains, more rain occurs.  On the lee side is a **rainshadow** with
  less rain)
* Climate change (some areas will start to get more rain, and other parts less)

### Difference Between Weather and Climate

**Weather** is day-to-day, short-term change in the atmosphere at a particular location.  Weather events are
often unexpected.  
**Climate** is the average of weather conditions measured over a long time.  
**Rainfall variability** is the way in which rainfall totals in a given area vary from year to year.  
**Relative humidity** is a measure of how much water vapour is in the air expressed as a percentage of the maximum amount of water that air could hold at that temperature.

### Global Water Use

* Agriculture (farming) uses 70% of the water
* Industry uses 20% of the water
* Domestic use is about 10% of the water

Dirty water with diseases kills about 3.3 million people every year around the world.  
**Virtual Water** is the amount of hidden water required to produce goods, for example, to produce a hamburge thats 2400 litres of hidden water.  

#### Water footprint

Is a measure of individual countries and the total volume of water that is used to produce
the goods and services.  It includes

* blue water (rivers, lakes, aquifers)
* green water (rainfall used for crops)
* grey water (water polluted by agricultural, industrial and household use)

## Water Scarcity

Water scarcity is when a large group of period, over a long period of time, do not have access to safe
and affordable water to satisfy their drinking, washing and livelihood needs.  Water scarcity is affected by

* Population (the amount of people)
* Agriculture (the choice of crops or animals and the amount of farming)
* Energy production, which can use a lot of water
* Climate change
* Private ownership

Steps we can take to increase water avaiability

* Having a water tank in every house
* Fixing all leaky pipes
* Using more groundwater
* Allocating more farming to where there is high rainfall
* Cloudseeding to make it rain more
* Using drip irrigation on crops
* Divert rivers back inland
* Move water from wet areas to dry areas (pipes, trucks)
* Reuse and recycle water
* Build more desalination plants
* Build more dams
* Increase the price of water to homes
* Cover reservoirs to stop evaporation
