# Place and Liveability

## A Sense of Place

Every location and region has its own identity that makes it different from other places.  The qualities that
might make it different are

* Natural features like harbours and mountains
* Human features: things built by humans

## Why People live in Certain Places

* **Pull factors** like availability of jobs, safety and climate draw people to places
* **Push factors** like cost, travel time and danger push people away from places

## Services and Facilities Important to Wellbeing

1. Transport and roads
1. Education
1. Water
1. Waste
1. Healthcare
1. Shopping centres
1. Cafes and restaurants

## How remoteness affects lifestyle

1. Travelling long distances to play sport
1. Big classrooms
1. Flying teachers and scientists (to teach special skills)
1. Shopping (mail order)
1. Flying doctors

## Factors that Influence Community Identity

1. Culture
1. The environment (eg. mountains, desert, coast, rainforest)
1. Public events
1. Religious events
1. Connectedness
   * Technology
   * Transport
   * Open Spaces

## Liveability

The following factors are used as criteria to rank how **liveable** cities are

* personal safety from crime, terror threats and civil unrest
* healthcare
* culture and environment (religious tolerance, corruption, climate and potential natural disasters)
* education
* infrastructure (transport, housing, energy, water and communication)
* economic stability
* recreational and sporting facilities
* availability of consumer goods (food, cars and household items)

Liveability is different to **sustainability**, which describes how well the community can continue
to provide for its population in the future and is affected by

* families not having to work long hours to pay for basic needs
* high renewable energy use (solar, hydro) and low non-renewable energy use (coal, oil)
* plenty of recycling and composting
* low vehicle kilometers travelled
* good water, forest and marine health
* high air quality
* biodiversity
* healthy living
* easy access to education
