# Summary

- [Assessment Scope](01.Assessment_Scope/HEADER.md)
- [Water in the World](02.Water_in_the_World/HEADER.md)
- [Place and Liveability](03.Place_and_Liveability/HEADER.md)
- [Interconnections](04.Interconnections/HEADER.md)
- [Geography Skills](05.Geography_Skills/HEADER.md)
  - [Topographic Maps](05.Geography_Skills/01.Topographic_Maps.md)
  - [Synoptic Charts](05.Geography_Skills/02.Synoptic_Charts.md)
  - [Population Pyramids](05.Geography_Skills/03.Population_Pyramids.md)
  - [Climate Graphs](05.Geography_Skills/04.Climate_Graphs.md)
