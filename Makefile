default: publish

test:
	./make_summary.sh
	markdownlint -c options.config src

publish: test clean
	mdbook build --dest-dir docs

serve: clean
	mdbook serve --dest-dir docs

clean:
	rm -rf docs
	rm -rf public
