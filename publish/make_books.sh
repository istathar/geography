#!/bin/bash

#-------------------------------------------------------------------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------------------------------------------------------------------
error () {
  echo $@     # Write the error
  cd ${CWD}   # Return to your starting directory
  exit 1      # Exit with error
}

noerror () {
  echo $@     # Write and exit message
  cd ${CWD}   # Return to your starting directory
  exit 0      # Exit with success
}

write_title_markdown () {
cat << EOF > ./${SAVEFILE_NAME}Title.markdown
\thispagestyle{title}

\AddToShipoutPictureBG*{
  \AtPageLowerLeft{
    \includegraphics[width=\paperwidth,height=\paperheight]{publish/images/${TITLE_IMAGE}}
  }
}

\vspace*{2cm}
\Huge
\sffamily

${TITLE_FUNCTION}

\vspace*{5cm}
\Huge
\sffamily

${TITLE_TITLE}

\Large

${PARTS}

\rmfamily
\normalsize

\vspace{7em}

\color{gray}

_v${TITLE_VERSION}_, ${TIMESTAMP} 

\color{black}

\vfill

\vfill

\large
\sffamily

${TITLE_TEAM}

\rmfamily
\normalsize

\clearpage

<!-- vim: set filetype=latex: -->
EOF
}

write_book_definition () {
  # Write the title and header information
  echo '% publish v2' > ./${SAVEFILE_NAME}.book
  echo 'publish/preamble.latex' >> ./${SAVEFILE_NAME}.book
  echo '% begin' >> ./${SAVEFILE_NAME}.book
  echo "publish/${SAVEFILE_NAME}Title.markdown" >> ./${SAVEFILE_NAME}.book 
  echo "publish/Contents.latex" >> ./${SAVEFILE_NAME}.book 
  
  # Delete any pre-existing parts files, they will be recreated as necessary
  rm -rf parts/*.latex
  
  # Write the actual parts of the book
  while read PIECE
  do
    if [[ $PIECE == *"PART.txt" ]]; then
      PARTTXT=$(head -n1 ${PIECE})
      PARTFIL=$(echo ${PARTTXT} | sed 's/ /_/g')
      if [ "${PARTTXT}" != "" ]; then
        printf "%s\n" "\\part{${PARTTXT}}" > publish/parts/${PARTFIL}.latex
        echo "publish/parts/${PARTFIL}.latex" >> ./${SAVEFILE_NAME}.book
      fi
    else
      echo $PIECE >> ./${SAVEFILE_NAME}.book
    fi
  done < mdfiles.tmp

  # Define any image files that are present
  grep -r '\!\[.*\](.*)' src/* > imgfiles.tmp
  while read IMG
  do
    IMGFILE=`echo ${IMG} | awk 'NR > 1 {print $1}' RS='(' FS=')'`  # Physical image file
    IMGREF=`echo ${IMG} | cut -f1 -d ':'`                          # Markdown file referencing the image file
    IMGDIR=$(dirname ${IMGREF})                                    # Directory where image path is relative to
    echo ${IMGDIR}/${IMGFILE} >> ./${SAVEFILE_NAME}.book
  done < imgfiles.tmp 

  # Write the title image to the book file
  echo "publish/images/${TITLE_IMAGE}" >> ./${SAVEFILE_NAME}.book

  # Delete temporary files
  rm -r mdfiles.tmp imgfiles.tmp

  # End off the book document
  echo '% end' >> ./${SAVEFILE_NAME}.book
}

#-------------------------------------------------------------------------------------------------------------------------------
# Main Code
#-------------------------------------------------------------------------------------------------------------------------------

# locale specified by the environment affects sort order; get the traditional sort order that uses native byte values.
export LC_ALL=C 

# Check that we have a jq executable
which jq > /dev/null 2>&1 || error "ERROR: jq is not installed.  Please install jq"

# Get the current working directory
CWD=`pwd`

# Get the full directory name of the script no matter where it is being called from.
SCRIPT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd ${SCRIPT_PATH}

find books -name "*.json" > /dev/null 2>&1 || noerror "INFO: nothing to do, no books/*.json book defintions to process"

# Work through each book definition file
for MANIFEST in books/*.json; do
  # Check if the json is valid
  cat ${MANIFEST} | jq > /dev/null
  [ $? -ne 0 ] && error "ERROR: Invalid JSON syntax in file ${MANIFEST}"
  TIMESTAMP=$(date '+%Y-%m-%d %H:%M %Z')
  TITLE_FUNCTION=`cat ${MANIFEST} | jq '."title-function"' | tr -d '"'` || error "ERROR: 'title-function' not defined in ${MANIFEST}"
  TITLE_TITLE=`cat ${MANIFEST} | jq '."title-title"' | tr -d '"'` || error "ERROR: 'title-title' not defined in ${MANIFEST}"
  TITLE_VERSION=`cat ${MANIFEST} | jq '."title-version"' | tr -d '"'` || error "ERROR: 'title-version' not defined in ${MANIFEST}"
  TITLE_TEAM=`cat ${MANIFEST} | jq '."title-team"' | tr -d '"'` || error "ERROR: 'title-team' not defined in ${MANIFEST}"
  TITLE_IMAGE=`cat ${MANIFEST} | jq '."title-image-name"' | tr -d '"'` || error "ERROR: 'title-image-name' not defined in ${MANIFEST}"
  if [ ! -f images/${TITLE_IMAGE} ]; then
    error "ERROR: error 'title-image-name' file images/${TITLE_IMAGE} does not exist"
  fi
  SAVEFILE_NAME=`cat ${MANIFEST} | jq '."savefile-name"' | tr -d '"'` || error "ERROR: 'savefile-name' not defined in ${MANIFEST}"

  PARTS=""
  EXCLUDE_PARTS=`cat ${MANIFEST} | jq '."exclude-parts"' | tr -d '"'` || error "ERROR: 'exclude-parts' not defined in ${MANIFEST}"
  if [ "${EXCLUDE_PARTS}" != "true" ]; then
    # If the book has parts then add them to the title
    find ../src -name "PART*" > parts.tmp
    while read FILE; do
      PART=`cat ${FILE}`
      PARTS="${PARTS}- ${PART}\n"
    done<parts.tmp    
    PARTS=$(echo -e "${PARTS}")
  fi
  rm -f parts.tmp
  write_title_markdown

  # Write the book structure, you have to do something funky to make sure it prints the headers and parts first
  cd ..
  find src  \( -name "*.md" -o -name "PART.txt" \) | \
    grep -v "SUMMARY.md" | \
    sed 's/HEADER.md/#HEADER.md/' | \
    sed 's/PART.txt/##PART.txt/'| \
    sort | \
    sed 's/#HEADER.md/HEADER.md/' | \
    sed 's/##PART.txt/PART.txt/' > mdfiles.tmp
  write_book_definition
  cd ${SCRIPT_PATH}
done

# Return to the directory you started from
cd ${CWD}
exit 0
